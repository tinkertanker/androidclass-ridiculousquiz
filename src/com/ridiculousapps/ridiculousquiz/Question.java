package com.ridiculousapps.ridiculousquiz;

public class Question {
	private int mStatement;
	private boolean mStatementTrue;
	
	public Question(int statement, boolean statementTrue) {
		mStatement = statement;
		mStatementTrue = statementTrue;
	}

	public int getStatement() {
		return mStatement;
	}

	public void setStatement(int statement) {
		mStatement = statement;
	}

	public boolean isStatementTrue() {
		return mStatementTrue;
	}

	public void setStatementTrue(boolean statementTrue) {
		mStatementTrue = statementTrue;
	}
}
