package com.ridiculousapps.ridiculousquiz;

import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HintActivity extends Activity {

	public static final String EXTRA_ISSTATEMENTTRUE = 
			"com.ridiculousapps.ridiculousquiz.is_statement_true";
	public static final String EXTRA_WASHINTGIVEN = 
			"com.ridiculousapps.ridiculousquiz.was_hint_given";
	
	private boolean mIsStatementTrue;
	private TextView mHintTextView;
	private Button mShowHintButton;
	private Random mGenerator;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hint);
		
		mGenerator = new Random();
		mIsStatementTrue = getIntent().getBooleanExtra(EXTRA_ISSTATEMENTTRUE, false);
		mHintTextView = (TextView)findViewById(R.id.hintTextView);
		mShowHintButton = (Button)findViewById(R.id.showHintButton);

		// No hint given unless the button was pressed and random number 
		// condition was met
		setHintGiven(false);
		
		mShowHintButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				int randomNumber = mGenerator.nextInt(100);
				if (randomNumber >= 50) {
					if (mIsStatementTrue) {
						mHintTextView.setText("The answer is true");
					} else {
						mHintTextView.setText("The answer is false");
					}
					// Hint was given in this case
					setHintGiven(true);
				} else {
					mHintTextView.setText("The capital of Zimbabwe is Harare.");
				}
				
			}
		});
		
	}
	
	private void setHintGiven(boolean wasHintGiven) {
		Intent data = new Intent();
		data.putExtra(EXTRA_WASHINTGIVEN, wasHintGiven);
		setResult(RESULT_OK, data);
	}
	
}
