package com.ridiculousapps.ridiculousquiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class QuizActivity extends Activity {
	
	private static final String KEY_CURRENTQUESTION = "current_question";
	private static final String KEY_NUMQUESTIONSATTEMPTED = "num_questions_attempted";
	private static final String KEY_NUMCORRECTANSWERS = "num_correct_answers";
	private static final String KEY_NUMHINTSGOTTEN = "num_hints_gotten";

	private Button mTrueButton;
	private Button mFalseButton;	
	private Button mNextButton;
	private Button mPreviousButton;
	private Button mGetHintButton;
	private TextView mQuestionTextView;
	private TextView mScoreTextView;
	private int mNumQuestionsAttempted = 0;
	private int mNumCorrectAnswers = 0;
	private int mNumHintsGotten = 0;
	
	private int mCurrentQuestion = 0;
	private Question[] mQuestionBank = new Question[] {
			new Question(R.string.question1, false),
			new Question(R.string.question2, false),
			new Question(R.string.question3, false),
			new Question(R.string.question4, false),
			new Question(R.string.question5, false),
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quiz);

		mQuestionTextView = (TextView)findViewById(R.id.question_text_view);
		updateQuestionText();
		
		mScoreTextView = (TextView)findViewById(R.id.score_text_view);
		mScoreTextView.setText("No questions answered yet");
		
		mTrueButton = (Button)findViewById(R.id.true_button);
		mTrueButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				checkUserAnswer(true);
			}
		});
		
		mFalseButton = (Button)findViewById(R.id.false_button);
		mFalseButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				checkUserAnswer(false);
			}
		});
		
		mNextButton = (Button)findViewById(R.id.next_button);
		mNextButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.d("QuizActivity", "Current question index is "+mCurrentQuestion);
				if (mCurrentQuestion < mQuestionBank.length - 1) {
					mCurrentQuestion++;
					updateQuestionText();
					enableButton(mPreviousButton, true);
					mPreviousButton.setText(R.string.previous_button);
				} 
				if (mCurrentQuestion >= mQuestionBank.length - 1) {
					enableButton(mNextButton, false);
					mNextButton.setText(R.string.no_more_questions);
				}
			}
		});
		
		mPreviousButton = (Button)findViewById(R.id.previous_button);
		enableButton(mPreviousButton, false);
		mPreviousButton.setText(R.string.no_more_questions);
		mPreviousButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.d("QuizActivity", "Current question index is "+mCurrentQuestion);
				if (mCurrentQuestion > 0) {
					mCurrentQuestion--;
					updateQuestionText();
					enableButton(mNextButton, true);
					mNextButton.setText(R.string.next_button);
				}
				if (mCurrentQuestion <= 0) {
					enableButton(mPreviousButton, false);
					mPreviousButton.setText(R.string.no_more_questions);
				}
				
			}
		});
		
		mGetHintButton = (Button)findViewById(R.id.get_hint_button);
		mGetHintButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(QuizActivity.this, HintActivity.class);
				boolean isCurrentStatementTrue = 
						mQuestionBank[mCurrentQuestion].isStatementTrue();
				i.putExtra(HintActivity.EXTRA_ISSTATEMENTTRUE, isCurrentStatementTrue);
				startActivityForResult(i, 0);
			}
		});
		
		if (savedInstanceState != null) {
			mCurrentQuestion = savedInstanceState.getInt(KEY_CURRENTQUESTION);
			mNumQuestionsAttempted = savedInstanceState.getInt(KEY_NUMQUESTIONSATTEMPTED);
			mNumCorrectAnswers = savedInstanceState.getInt(KEY_NUMCORRECTANSWERS);
			mNumHintsGotten = savedInstanceState.getInt(KEY_NUMHINTSGOTTEN);
			updateScoreText();
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.quiz, menu);
		return true;
	}
	
	private void updateQuestionText() {
		int currentQuestion = mQuestionBank[mCurrentQuestion].getStatement();
		mQuestionTextView.setText(currentQuestion);
	}
	
	private void updateScoreText() {
		String scoreString = getResources().getString(R.string.score_percentage, 
				mNumCorrectAnswers, mNumQuestionsAttempted, mNumHintsGotten);
		this.mScoreTextView.setText(scoreString);
	}
	
	private void checkUserAnswer(boolean userSelectedTrue) {
		int toastMessage;
		
		boolean isAnswerTrue = mQuestionBank[mCurrentQuestion].isStatementTrue(); 
		if (userSelectedTrue == isAnswerTrue) {
			toastMessage = R.string.correct_message;
			mNumCorrectAnswers++;
		} else {
			toastMessage = R.string.wrong_message;
		}
		mNumQuestionsAttempted++;
		Toast.makeText(QuizActivity.this, toastMessage, Toast.LENGTH_SHORT).show();
		updateScoreText();
	}
	
	private void enableButton(Button button, boolean enable) {
		if (enable) {
			button.setClickable(true);
			button.setAlpha(1.0f);
		} else {
			button.setClickable(false);
			button.setAlpha(0.5f);			
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putInt(KEY_CURRENTQUESTION, mCurrentQuestion);
		savedInstanceState.putInt(KEY_NUMQUESTIONSATTEMPTED, mNumQuestionsAttempted);
		savedInstanceState.putInt(KEY_NUMCORRECTANSWERS, mNumCorrectAnswers);
		savedInstanceState.putInt(KEY_NUMHINTSGOTTEN, mNumHintsGotten);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data == null) {
			return;
		}
		boolean gotHint = data.getBooleanExtra(HintActivity.EXTRA_WASHINTGIVEN, false);
		if (gotHint) {
			mNumHintsGotten++;
		}
		updateScoreText();
	}

}
